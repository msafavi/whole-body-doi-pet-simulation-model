// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************

//GEANT4 - Depth-of-Interaction enabled Positron emission tomography (PET) advanced example 

//Authors and contributors

// Author list to be updated, with names of co-authors and contributors from National Institute of Radiological Sciences (NIRS)

// Abdella M. Ahmed (1, 2), Andrew Chacon (1, 2), Harley Rutherford (1, 2),
// Hideaki Tashima (3), Go Akamatsu (3), Akram Mohammadi (3), Eiji Yoshida (3), Taiga Yamaya (3)
// Susanna Guatelli (2), and Mitra Safavi-Naeini (1, 2)

// (1) Australian Nuclear Science and Technology Organisation, Australia
// (2) University of Wollongong, Australia
// (3) National Institute of Radiological Sciences, Japan



//#include "doiPETGlobalParameters.hh"
#include "doiPETDetectorConstruction.hh"
#include "doiPETPhysicsList.hh"
#include "doiPETAnalysis.hh"
#include "doiPETActionInitialization.hh"

#include "Randomize.hh"
#include "G4UImanager.hh"
#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"
#include "G4SystemOfUnits.hh"
#include <time.h>

#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif

#include "G4RunManager.hh"
//
#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif
//
//////////////////////////////////////////////////////////////////////////////

int main(int argc,char** argv)
{
	clock_t t1,t2; //for timing
	t1=clock();

	G4UIExecutive* ui = 0;
	if ( argc == 1 ) {
    ui = new G4UIExecutive(argc, argv);
	}
	
	// Choose the Random engine
	G4Random::setTheEngine(new CLHEP::RanecuEngine);
	
#ifdef G4MULTITHREADED

	G4MTRunManager* runManager = new G4MTRunManager;
	runManager->SetNumberOfThreads(1); // Is equal to 2 by default
#else

	G4RunManager* runManager = new G4RunManager;
#endif
	
	//G4RunManager* runManager = new G4RunManager;

	
	G4double RandSeed;
	G4cout << "Enter a number for the random seed" <<G4endl;  
	G4cin >> RandSeed;
	G4long seed = time(NULL);
	CLHEP::HepRandom::setTheSeed(seed*RandSeed);
	//
	runManager->SetUserInitialization(new doiPETDetectorConstruction());

	runManager->SetUserInitialization(new doiPETPhysicsList());

	// Set user action initialization
	runManager->SetUserInitialization(new doiPETActionInitialization());

	//Initialize analysis
	doiPETAnalysis* ptrAnalysis = doiPETAnalysis::GetInstance();


	G4double act  = 1000000 * becquerel;//Activity is set via run.mac file
	ptrAnalysis->SetActivity(act);

	G4double halfLife = 109.771 * 60 * s; //Halflife of F-18 as a default
	ptrAnalysis -> SetIsotopeHalfLife(halfLife);

	//Blurring specification of the scanner. see inputParameter.txt
	ptrAnalysis -> BlurringParameters();

	//Open file to write the output of the simulation
	ptrAnalysis->Open("result"); //file extention is affixed based on the type of the output (.root for root or .data for ascii)


	//Read reflector pattern from the inputParameter.txt file
	ptrAnalysis->ReadReflectorPattern();

	// Get the pointer to the User Interface manager
	G4UImanager* UImanager = G4UImanager::GetUIpointer();

  if ( ! ui ) { 
    // batch mode
    G4String command = "/control/execute ";
    G4String fileName = argv[1];
    UImanager->ApplyCommand(command+fileName);
  }
  else { 
    // interactive mode
	G4VisManager* visManager = new G4VisExecutive;
    visManager->Initialize();
    UImanager->ApplyCommand("/control/execute init_vis.mac");
    ui->SessionStart();
    delete ui;
	delete visManager;
  }

	//close the file
	ptrAnalysis->Close();
	ptrAnalysis->Delete();
	
	delete runManager;
	t2=clock();
	G4cout<<"Elapsed time (main) : " <<(long double)((t2-t1)/CLOCKS_PER_SEC)/60<<" min"<<G4endl;
	return 0;

}

//////////////////////////////////////////////////////////////////////////////

